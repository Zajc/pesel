package pl.sdacademy.day12.pesel;


import java.util.Scanner;

public class Pesel {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        String pesel = setPesel();
        if (isPeselCorrect(pesel)) {
            getAge(pesel);
            getSex(pesel);
            getNumber(pesel);
        } else {
            System.out.println("Podano błędny PESEL!!");
        }
    }

    private static boolean isPeselCorrect(String pesel) {
        int[] scales = {1, 3, 7, 9, 1, 3, 7, 9, 1, 3};
        int calculate = 0;
        for (int i = 0; i < 10; i++) {
            calculate += ((scales[i] * ((int) pesel.charAt(i)-48)));
        }
        calculate = 1000 - calculate;
        String last = Integer.toString(calculate);
        return last.charAt(last.length() - 1) == pesel.charAt(10);
    }

    private static String setPesel() {
        String pesel;
        do {
            System.out.println("Podaj PESEL: ");
            pesel = scanner.nextLine();
        } while (pesel.length() != 11);
        return pesel;
    }

    private static void getNumber(String pesel) {
        System.out.println("Jesteś " + pesel.charAt(6) + pesel.charAt(7) + pesel.charAt(8) + " osobą o tym imieniu i nazwisku,");
    }

    private static void getSex(String pesel) {
        if (pesel.charAt(9) % 2 == 0) {
            System.out.println("Płeć: Kobieta");
        } else {
            System.out.println("Płeć: Mężczyzna");
        }
    }

    private static void getAge(String pesel) {
        System.out.println("Data Urodzenia ");
        String month;
        if (isFrom18XX(pesel)) {
            if (pesel.charAt(2) == '8') {
                month = getMonth(pesel.charAt(3));
            } else {
                month = getMonthPlus(pesel.charAt(3));
            }
            System.out.println("Rok: 18" + pesel.charAt(0) + pesel.charAt(1) + ", Miesiąc: " + month + ", Dzień: ");
        }
        if (isFrom19XX(pesel)) {
            if (pesel.charAt(2) == '0') {
                month = getMonth(pesel.charAt(3));
            } else {
                month = getMonthPlus(pesel.charAt(3));
            }
            System.out.println("Rok: 19" + pesel.charAt(0) + pesel.charAt(1) + ", Miesiąc: " + month + ", Dzień: ");
        }
        if (isFrom20XX(pesel)) {
            if (pesel.charAt(2) == '2') {
                month = getMonth(pesel.charAt(3));
            } else {
                month = getMonthPlus(pesel.charAt(3));
            }
            System.out.println("Rok: 20" + pesel.charAt(0) + pesel.charAt(1) + ", Miesiąc: " + month + ", Dzień: ");
        }
    }
    private static boolean isFrom18XX(String pesel){
        return pesel.charAt(2) == '8' || pesel.charAt(2) == '9';
    }
    private static boolean isFrom19XX(String pesel){
        return pesel.charAt(2) == '0' || pesel.charAt(2) == '1';
    }
    private static boolean isFrom20XX(String pesel){
        return pesel.charAt(2) == '2' || pesel.charAt(2) == '3';
    }

    private static String getMonth(char peselChar) {
        String month = null;
        switch (peselChar) {
            case '1':
                month = "Styczeń";
                break;
            case '2':
                month = "Luty";
                break;
            case '3':
                month = "Marzec";
                break;
            case '4':
                month = "Kwiecień";
                break;
            case '5':
                month = "Maj";
                break;
            case '6':
                month = "Cerwiec";
                break;
            case '7':
                month = "Lipiec";
                break;
            case '8':
                month = "Sierpień";
                break;
            case '9':
                month = "Wrzesień";
                break;
        }
        return month;
    }

    private static String getMonthPlus(char peselChar) {
        String month = null;
        switch (peselChar) {
            case '0':
                month = "Październik";
                break;
            case '1':
                month = "Listopad";
                break;
            case '2':
                month = "Grudzień";
                break;
        }
        return month;
    }
}
